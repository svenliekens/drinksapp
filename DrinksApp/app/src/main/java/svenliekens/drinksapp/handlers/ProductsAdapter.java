package svenliekens.drinksapp.handlers;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.models.Product;

/**
 * Created by Sven on 20/01/2018.
 */

/**
 * Custom ArrayAdapter to show products in ListView
 */
public class ProductsAdapter extends ArrayAdapter<Product> {
    private ArrayList<Product> products;
    private Context context;
    private int resource;

    public ProductsAdapter(Context context, int resource, ArrayList<Product> products) {
        super(context, resource, products);

        this.products = products;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // if the view is null, get it from the activity
        if (convertView == null) {
            convertView = ((LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(resource, parent, false);
        }

        // get the needed product
        Product product = products.get(position);

        // create textviews
        if(product != null){
            fillTextView(convertView, R.id.product_name, product.getName(), "No name");
            fillTextView(convertView, R.id.product_amount, String.valueOf(product.getAmount()), "No amount");
            fillTextView(convertView, R.id.product_price, "€" + String.valueOf(product.getPrice()), "No price");
        }
        return convertView;
    }

    private void fillTextView(View convertView, int viewID, String content, String defaultContent) {
        // get textview from view
        TextView tv = (TextView) convertView.findViewById(viewID);

        if(tv != null){
            // if there is a content, set it, else set the default content
            if (content != null && content.length() > 0) {
                tv.setText(content);
            } else {
                tv.setText(defaultContent);
            }
        }
    }
}
