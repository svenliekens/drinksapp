package svenliekens.drinksapp.handlers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Random;

import svenliekens.drinksapp.MainActivity;
import svenliekens.drinksapp.R;
import svenliekens.drinksapp.listeners.UserDataChangedListener;
import svenliekens.drinksapp.models.Product;
import svenliekens.drinksapp.models.User;

/**
 * Created by Sven on 21/01/2018.
 */

/**
 * Custom WidgetProvider to provide app widget.
 * This widget is used to check the amount to pay while on the home screen.
 */
public class PaymentWidgetProvider extends AppWidgetProvider{
    private DbHandler db;
    private Double amountToPay = 0.0;
    private Context c;
    private FirebaseAuth auth;

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
        c = context;
        final int count = appWidgetIds.length;

        db = DbHandler.getInstance();
        auth = FirebaseAuth.getInstance();

        // If user is not logged in
        if(auth.getCurrentUser() == null){
            for (int i = 0; i < count; i++) {
                int widgetId = appWidgetIds[i];

                // Show not logged in in widget
                RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
                remoteViews.setTextViewText(R.id.widget_to_pay_label, "Not logged in");
                remoteViews.setTextViewText(R.id.amount_to_pay_textview, "");

                Intent in = new Intent(context, MainActivity.class);
                PendingIntent pI = PendingIntent.getActivity(context, 0, in, 0);
                remoteViews.setOnClickPendingIntent(R.layout.widget_layout, pI);

                // Refresh widget when refresh buton is clicked
                Intent intent = new Intent(context, PaymentWidgetProvider.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                        0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.refresh_button, pendingIntent);

                // Open app when widget is clicked
                Intent openApp = new Intent(context, MainActivity.class);

                PendingIntent appIntent = PendingIntent.getActivity(context, 0, openApp, 0);
                remoteViews.setOnClickPendingIntent(R.id.widget_layout, appIntent);

                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }else{
            // If user is logged in
            // Get user data
            db.getUserDataOnce(new UserDataChangedListener() {
                @Override
                public void userDataChanged(User u) {
                    // Calculate amount to pay from user's products
                    ArrayList<Product> products = u.getProducts();
                    for(Product p : products){
                        if(p.getAmount() != 0){
                            amountToPay += (((double)p.getAmount())* p.getPrice());
                        }
                    }

                    for (int i = 0; i < count; i++) {
                        int widgetId = appWidgetIds[i];

                        // Show amount to pay
                        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
                        remoteViews.setTextViewText(R.id.amount_to_pay_textview, "€"+String.valueOf(amountToPay));
                        remoteViews.setTextViewText(R.id.widget_to_pay_label, "To pay:");

                        Intent in = new Intent(context, MainActivity.class);
                        PendingIntent pI = PendingIntent.getActivity(context, 0, in, 0);
                        remoteViews.setOnClickPendingIntent(R.layout.widget_layout, pI);

                        // Refresh widget when pressing refresh button
                        Intent intent = new Intent(context, PaymentWidgetProvider.class);
                        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        remoteViews.setOnClickPendingIntent(R.id.refresh_button, pendingIntent);

                        // Open app when widget is clicked
                        Intent openApp = new Intent(context, MainActivity.class);

                        PendingIntent appIntent = PendingIntent.getActivity(context, 0, openApp, 0);
                        remoteViews.setOnClickPendingIntent(R.id.widget_layout, appIntent);

                        appWidgetManager.updateAppWidget(widgetId, remoteViews);
                    }
                }
            });
        }

    }

}
