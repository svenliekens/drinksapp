package svenliekens.drinksapp.handlers;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import svenliekens.drinksapp.listeners.GroupDataChangedListener;
import svenliekens.drinksapp.listeners.GroupProductsChangedListener;
import svenliekens.drinksapp.listeners.ProductsListChangedListener;
import svenliekens.drinksapp.listeners.UserDataChangedListener;
import svenliekens.drinksapp.models.Product;
import svenliekens.drinksapp.models.Group;
import svenliekens.drinksapp.models.User;

/**
 * Created by Sven on 20/01/2018.
 */


/**
 * This singleton class is used to execute all database actions.
 * It connects the application to the Firebase Realtime Database and Firebase Authentication
 */
public class DbHandler {
    private static DbHandler instance = null;

    private FirebaseDatabase db;
    private FirebaseAuth auth;

    protected DbHandler(){
        this.db = FirebaseDatabase.getInstance();
        this.auth = FirebaseAuth.getInstance();
    }

    public static DbHandler getInstance(){
        if(instance == null){
            instance = new DbHandler();
        }
        return instance;
    }

    // ----------------------------- GROUP -----------------------------

    /**
     * Method to sync the group's products with the user's products (price update, product added, product deleted)
     */
    public void syncGroupProductsWithUser(final GroupProductsChangedListener l) {
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId()).child("products");
                groupProductsRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<ArrayList<Product>> t = new GenericTypeIndicator<ArrayList<Product>>() {};
                        ArrayList<Product> groupProducts = dataSnapshot.getValue(t);
                        l.groupProductsChanged(groupProducts);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    /**
     * Method to obtain the group's products once
     */
    public void getGroupProductsOnce(final ProductsListChangedListener l){
        FirebaseUser currentUser = auth.getCurrentUser();

        if(currentUser != null){
            getUserDataOnce(new UserDataChangedListener() {
                @Override
                public void userDataChanged(User u) {
                    DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId());
                    if(groupProductsRef != null){
                        groupProductsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Group products = dataSnapshot.getValue(Group.class);
                                if(products != null){
                                    l.productsListChanged(products.getProducts());
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * Method to link the listener for group products (used in EditGroupProductsFragment to update product list)
     */
    public void linkGroupProductsListener(final ProductsListChangedListener l){
        FirebaseUser currentUser = auth.getCurrentUser();

        if(currentUser != null){
            getUserDataOnce(new UserDataChangedListener() {
                @Override
                public void userDataChanged(User u) {
                    DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId());
                    if(groupProductsRef != null){
                        groupProductsRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Group products = dataSnapshot.getValue(Group.class);
                                if(products != null){
                                    l.productsListChanged(products.getProducts());
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * Method to create a new group
     * @param groupName
     * @param date
     * @param groupPassword
     * @param year
     * @param month
     */
    public void createGroup(final String groupName, String groupPassword, int year, int month, int date){
        String payDate = date + "/" + month + "/" + year;
        final Group group = new Group(groupName.trim(), groupPassword.trim(), payDate);

        final DatabaseReference productListRef = db.getReference("lists").child(group.getGroupName());

        productListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object val = dataSnapshot.getValue();
                if(val == null){
                    productListRef.setValue(group);

                    DatabaseReference userRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("listId");
                    userRef.setValue(groupName);

                    resetUserProductsToGroupProducts();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method to join an existing group
     * @param groupName
     * @param groupPassword
     */
    public void joinGroup(String groupName, final String groupPassword){
        DatabaseReference groupToJoinRef = db.getReference("lists").child(groupName);
        groupToJoinRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Group list = dataSnapshot.getValue(Group.class);
                if(list != null){
                    if(list.getPassword().equals(groupPassword)){
                        DatabaseReference userListReference = db.getReference("users").child(auth.getCurrentUser().getUid()).child("listId");
                        userListReference.setValue(list.getGroupName());

                        resetUserProductsToGroupProducts();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method to obtain the group's data once (used in ManageGroupFragment to get payDate)
     */
    public void getGroupDataOnce(final GroupDataChangedListener l){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                DatabaseReference groupRef = db.getReference("lists").child(u.getListId());
                groupRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        l.groupDataChanged(dataSnapshot.getValue(Group.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    /**
     * Method to link listener for receiving an event when the group's paydate has changed
     */
    public void linkGroupPaydateChangedListener(final GroupDataChangedListener l){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                DatabaseReference groupRef = db.getReference("lists").child(u.getListId());
                groupRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        l.groupDataChanged(dataSnapshot.getValue(Group.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    /**
     * Method to update the group's paydate
     * @param payDate
     */
    public void updateGroupData(final String payDate){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                DatabaseReference groupNameRef = db.getReference("lists").child(u.getListId());
                groupNameRef.child("payDate").setValue(payDate);
            }
        });
    }

    /**
     * Method to delete a product from the group products
     * @param productToDelete Product to delete from group
     */
    public void deleteGroupProduct(final Product productToDelete){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                final DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId()).child("products");
                getGroupProductsOnce(new ProductsListChangedListener() {
                    @Override
                    public void productsListChanged(ArrayList<Product> productList) {
                        ArrayList<Product> toRemove = new ArrayList<>();
                        for (Product p : productList) {
                            if(p.getName().equals(productToDelete.getName())){
                                toRemove.add(p);
                            }
                        }
                        productList.removeAll(toRemove);

                        groupProductsRef.setValue(productList);
                    }
                });
            }
        });
    }

    /**
     * Method to add a new product to the group products
     * @param productToAdd Product to be added to the group
     */
    public void addGroupProduct(final Product productToAdd){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                final DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId()).child("products");
                getGroupProductsOnce(new ProductsListChangedListener() {
                    @Override
                    public void productsListChanged(ArrayList<Product> productList) {
                        boolean productAlreadyExists = false;

                        for(Product p: productList){
                            if(productToAdd.getName().equals(p.getName())){
                                productAlreadyExists = true;
                            }
                        }

                        if(!productAlreadyExists){
                            productList.add(productToAdd);
                            groupProductsRef.setValue(productList);
                        }
                    }
                });
            }
        });
    }

    /**
     * Method to update a group product (example: update price)
     * @param updatedProduct
     */
    public void updateGroupProduct(final Product updatedProduct){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                final DatabaseReference groupProductsRef = db.getReference("lists").child(u.getListId()).child("products");
                getGroupProductsOnce(new ProductsListChangedListener() {
                    @Override
                    public void productsListChanged(ArrayList<Product> productList) {
                        int indexToUpdate = -1;

                        for(int i = 0; i<productList.size(); i++){
                            if(updatedProduct.getName().equals(productList.get(i).getName())){
                                indexToUpdate = i;
                                break;
                            }
                        }

                        if(indexToUpdate > -1){
                            DatabaseReference productRef = groupProductsRef.child(String.valueOf(indexToUpdate));
                            productRef.setValue(updatedProduct);
                        }
                    }
                });
            }
        });
    }

    /**
     * Method to prompt every group member to reset the amount to pay (group owner not included)
     */
    public void resetPaymentsInGroup(){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(final User u) {
                Query allGroupMembers = db.getReference("users").orderByChild("listId").equalTo(u.getListId());
                allGroupMembers.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        GenericTypeIndicator<HashMap<String,User>> t = new GenericTypeIndicator<HashMap<String, User>>(){};
                        HashMap<String, User> usersInGroup = dataSnapshot.getValue(t);

                        for(Map.Entry<String, User> entry: usersInGroup.entrySet()){
                            String key = entry.getKey();
                            User user = entry.getValue();

                            if(!key.equals(u.getuId())){
                                user.setState(User.UserState.PAYMENT_RECEIVED);
                                entry.setValue(user);
                            }
                        }

                        Map map = Collections.synchronizedMap(usersInGroup);
                        db.getReference("users").updateChildren(map);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    // ----------------------------- USER -----------------------------

    /**
     * Method to copy the group's products to the user (used to reset product amounts)
     */
    public void resetUserProductsToGroupProducts() {
        getGroupProductsOnce(new ProductsListChangedListener() {
            @Override
            public void productsListChanged(ArrayList<Product> productList) {
                DatabaseReference userProductsRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("products");
                userProductsRef.setValue(productList);
            }
        });
    }

    /**
     * Method used to link listener that fires an event when the user's products have changed (example: product amount updated)
     * @param l
     */
    public void linkUserProductsListener(final ProductsListChangedListener l){
        DatabaseReference userProductsRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("products");
        userProductsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Product>> t = new GenericTypeIndicator<ArrayList<Product>>() {};
                ArrayList<Product> productList = dataSnapshot.getValue(t);
                l.productsListChanged(productList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method to link user data listener that fires an event when the user's data has changed (example: user state changes to change view)
     * @param l
     */
    public void linkUserDataListener(final UserDataChangedListener l){
        FirebaseUser currentUser = auth.getCurrentUser();

        if(currentUser != null){
            DatabaseReference userStateRef = db.getReference("users").child(currentUser.getUid());
            if(userStateRef != null){
                userStateRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if(user != null){
                            l.userDataChanged(user);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    /**
     * Method to set the user state (used to change the user's view/fragment)
     * @param state
     */
    public void setUserState(User.UserState state){
        DatabaseReference userStateRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("state");
        if(userStateRef != null){
            userStateRef.setValue(state);
        }
    }

    /**
     * Method to obtain the user's data once (example: display group name in GroupOwnerHomePageFragment)
     * @param l
     */
    public void getUserDataOnce(final UserDataChangedListener l){
        FirebaseUser currentUser = auth.getCurrentUser();

        if(currentUser != null){
            DatabaseReference userStateRef = db.getReference("users").child(currentUser.getUid());
            if(userStateRef != null){
                userStateRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if(user != null){
                            l.userDataChanged(user);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    /**
     * Method to update the user's products
     * @param userProducts
     */
    public void updateUserProducts(ArrayList<Product> userProducts){
        DatabaseReference userProductsRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("products");
        userProductsRef.setValue(userProducts);
    }

    /**
     * Method to update one user product (example: product amount updated)
     * @param updatedProduct
     */
    public void updateUserProduct(final Product updatedProduct){
        getUserDataOnce(new UserDataChangedListener() {
            @Override
            public void userDataChanged(User u) {
                DatabaseReference userProductsRef = db.getReference("users").child(auth.getCurrentUser().getUid()).child("products");
                ArrayList<Product> userProducts = u.getProducts();

                int indexToUpdate = -1;

                for(int i = 0; i<userProducts.size(); i++){
                    if(updatedProduct.getName().equals(userProducts.get(i).getName())){
                        indexToUpdate = i;
                        break;
                    }
                }

                if(indexToUpdate > -1){
                    DatabaseReference productRef = userProductsRef.child(String.valueOf(indexToUpdate));
                    productRef.setValue(updatedProduct);
                }
            }
        });
    }
}