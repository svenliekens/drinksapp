package svenliekens.drinksapp.listeners;

import svenliekens.drinksapp.models.User;

/**
 * Created by Sven on 19/01/2018.
 */

public interface UserDataChangedListener {
    void userDataChanged(User u);
}
