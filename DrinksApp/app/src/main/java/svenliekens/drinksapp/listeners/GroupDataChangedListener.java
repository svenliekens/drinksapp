package svenliekens.drinksapp.listeners;

import svenliekens.drinksapp.models.Group;

/**
 * Created by Sven on 20/01/2018.
 */

public interface GroupDataChangedListener {
    void groupDataChanged(Group group);
}
