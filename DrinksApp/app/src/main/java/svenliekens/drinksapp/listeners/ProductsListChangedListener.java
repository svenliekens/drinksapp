package svenliekens.drinksapp.listeners;

import java.util.ArrayList;

import svenliekens.drinksapp.models.Product;

/**
 * Created by Sven on 20/01/2018.
 */

public interface ProductsListChangedListener {
    void productsListChanged(ArrayList<Product> productList);
}
