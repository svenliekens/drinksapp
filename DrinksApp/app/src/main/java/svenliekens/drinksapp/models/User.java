package svenliekens.drinksapp.models;

import java.util.ArrayList;

/**
 * Created by Sven on 19/01/2018.
 */

/**
 * Model for User
 */
public class User {
    // Enum with all possible user states
    // This is used to set the view that is being displayed
    public enum UserState{
        CREATE_GROUP, JOIN_GROUP, NOT_ASSIGNED, GROUP_OWNER_HOMEPAGE, MANAGE_GROUP, GROUP_OWNER_PRODUCTS_LIST, PRODUCTS_LIST, EDIT_GROUP_PRODUCTS, ADD_GROUP_PRODUCT, PAYMENT_RECEIVED
    };

    private String name;
    private String uId;
    private UserState state;
    private String listId;
    private ArrayList<Product> products;

    public User(){}

    public User(String uId, String name){
        this.uId = uId;
        this.name = name;
        this.state = UserState.NOT_ASSIGNED;
        this.listId = "-1";
        this.products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public UserState getState() {
        return state;
    }

    public void setState(UserState state) {
        this.state = state;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
