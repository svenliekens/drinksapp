package svenliekens.drinksapp.models;

import java.io.Serializable;

/**
 * Created by Sven on 19/01/2018.
 */

/**
 * Model for Product
 */
public class Product implements Serializable {
    private String name;
    private double price;
    private int amount;

    public Product(){}

    public Product(String name, double price){
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
