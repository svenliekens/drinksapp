package svenliekens.drinksapp.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sven on 20/01/2018.
 */

/**
 * Model for group
 */
public class Group {
    private String password;
    private String groupName;
    private String payDate;
    private ArrayList<Product> products;

    public Group(){}

    public Group(String groupName, String password, String payDate){
        this.groupName = groupName;
        this.password = password;
        this.products = new ArrayList<>();
        this.payDate = payDate;

        // Add default set of products to each group
        products.add(new Product("Coca-Cola", 1.5));
        products.add(new Product("Fanta", 1.5));
        products.add(new Product("Ice tea", 2.0));
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }
}
