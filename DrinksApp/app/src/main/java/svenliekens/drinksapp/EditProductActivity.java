package svenliekens.drinksapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.Product;

/**
 * Activity for group owner. This activity is used to edit a group product (examples: update price, delete product)
 */
public class EditProductActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView priceTextView;
    private TextView productNameTextView;
    private EditText productPriceEditText;

    private DbHandler db;

    private Product p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        db = DbHandler.getInstance();

        priceTextView = findViewById(R.id.product_price_edittext);
        productNameTextView = findViewById(R.id.product_name_textview);
        productPriceEditText = findViewById(R.id.product_price_edittext);

        (findViewById(R.id.delete_product_button)).setOnClickListener(this);
        (findViewById(R.id.save_product_button)).setOnClickListener(this);

        // Get product to edit from Intent extras
        p = (Product) getIntent().getExtras().getSerializable("product");

        // Display product data
        if(p != null){
            priceTextView.setText(String.valueOf(p.getPrice()));
            productNameTextView.setText(String.valueOf(p.getName()));
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.delete_product_button:
                // Delete product using DbHandler
                db.deleteGroupProduct(p);
                goBack();
                break;

            case R.id.save_product_button:
                // Get product price from UI
                String productPriceString = productPriceEditText.getText().toString().trim();

                // Check if product price is empty
                if(productPriceString.isEmpty()){
                    Toast.makeText(this, "Price field can't be empty!", Toast.LENGTH_SHORT).show();
                }else{
                    // Create updatedProduct from input data
                    Product updatedProduct = new Product(p.getName(), Double.parseDouble(productPriceString));
                    // Update group product using DbHandler
                    db.updateGroupProduct(updatedProduct);
                    // Navigate back to EditGroupProductsFragment
                    goBack();
                }

                break;
        }
    }

    /**
     * Override back button press to go back to EditGroupProductsFragment on back pressed
     */
    @Override
    public void onBackPressed() {
        goBack();
    }

    /**
     * Method to go back to mainactivity which should still be displaying the EditGroupProductsFragment
     */
    private void goBack() {
        // Kill this activity
        finish();
        // Nav to MainActivity
        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        startActivity(mainActivityIntent);
    }
}
