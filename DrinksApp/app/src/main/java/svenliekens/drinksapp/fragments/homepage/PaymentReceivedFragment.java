package svenliekens.drinksapp.fragments.homepage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.User;

/**
 * This is a fragment for a user that is registered and in a group.
 * It is used to let the user know if the group owner has received their payment.
 */
public class PaymentReceivedFragment extends Fragment implements View.OnClickListener{

    private DbHandler db;

    public PaymentReceivedFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_payment_received, container, false);

        v.findViewById(R.id.payment_received_ok_button).setOnClickListener(this);

        db = DbHandler.getInstance();

        return v;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.payment_received_ok_button){
            // Reset user's products when user has confirmed message
            db.resetUserProductsToGroupProducts();
            // Navigate to products list
            db.setUserState(User.UserState.PRODUCTS_LIST);
        }
    }
}
