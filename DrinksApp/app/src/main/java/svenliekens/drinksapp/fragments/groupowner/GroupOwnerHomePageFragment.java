package svenliekens.drinksapp.fragments.groupowner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.listeners.UserDataChangedListener;
import svenliekens.drinksapp.models.User;

/**
 * Fragment for the group owner. This is the homepage for a group owner.
 */
public class GroupOwnerHomePageFragment extends Fragment implements View.OnClickListener, UserDataChangedListener {
    private DbHandler db;

    private TextView groupNameTextView;

    public GroupOwnerHomePageFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_group_owner_home_page, container, false);

        db = DbHandler.getInstance();

        v.findViewById(R.id.manage_group_button).setOnClickListener(this);
        v.findViewById(R.id.my_products_button).setOnClickListener(this);

        groupNameTextView = v.findViewById(R.id.group_name_textview);

        // Get the user's data once and make this class listener for this event
        db.getUserDataOnce(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.manage_group_button:
                // Navigate to ManageGroupFragment when manage group button is pressed
                db.setUserState(User.UserState.MANAGE_GROUP);
                break;

            case R.id.my_products_button:
                // Navigate to ProductsListFragment when my products button is pressed
                db.setUserState(User.UserState.GROUP_OWNER_PRODUCTS_LIST);
                break;
        }
    }

    @Override
    public void userDataChanged(User u) {
        // Update the view with the group name (result of getUserDataOnce in onCreateView)
        groupNameTextView.setText(u.getListId());
    }
}