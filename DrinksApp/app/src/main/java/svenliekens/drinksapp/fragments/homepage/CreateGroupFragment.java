package svenliekens.drinksapp.fragments.homepage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.User;

/**
 * This is a fragment for newly registered users. This fragment is used to create a new group
 */
public class CreateGroupFragment extends Fragment implements View.OnClickListener {

    private DbHandler db;
    private EditText groupNameEditText;
    private EditText groupPasswordEditText;
    private EditText dayEditText;
    private EditText monthEditText;
    private EditText yearEditText;

    public CreateGroupFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_creategroup, container, false);

        groupNameEditText = v.findViewById(R.id.group_name_edittext);
        groupPasswordEditText = v.findViewById(R.id.group_password_edittext);
        dayEditText = v.findViewById(R.id.create_group_day_edittext);
        monthEditText = v.findViewById(R.id.create_group_month_edittext);
        yearEditText = v.findViewById(R.id.create_group_year_edittext);

        v.findViewById(R.id.create_group_button).setOnClickListener(this);

        db = DbHandler.getInstance();

        return v;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.create_group_button){
            //Create new group after pressing new group button
            //Get all data necessary to create new group from UI
            String groupName = groupNameEditText.getText().toString().trim();
            String groupPassword = groupPasswordEditText.getText().toString();
            String dayString = dayEditText.getText().toString().trim();
            String monthString = monthEditText.getText().toString().trim();
            String yearString = yearEditText.getText().toString().trim();

            // Check if any of the fields are empty
            if(groupName.isEmpty() || groupPassword.isEmpty() || dayString.isEmpty() || monthString.isEmpty() || yearString.isEmpty()){
                // Show toast if any is empty
                Toast.makeText(getContext(), "All fields must be filled in!", Toast.LENGTH_SHORT).show();
            }else{
                // Convert input strings to numbers
                int year = Integer.parseInt(yearString);
                int month = Integer.parseInt(monthString);
                int day = Integer.parseInt(dayString);

                // Create new group using DbHandler
                db.createGroup(groupName, groupPassword, year, month, day);

                // Navigate to GroupOwnerHomePageFragment
                db.setUserState(User.UserState.GROUP_OWNER_HOMEPAGE);
            }
        }
    }
}
