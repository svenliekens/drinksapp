package svenliekens.drinksapp.fragments.homepage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.User;

/**
 * This is a fragment for a newly registered user. This fragment is used to join an existing group.
 */
public class JoinGroupFragment extends Fragment implements View.OnClickListener{

    private EditText groupNameEditText;
    private EditText groupPasswordEditText;

    private DbHandler db;

    public JoinGroupFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_join_group, container, false);

        v.findViewById(R.id.join_group_button).setOnClickListener(this);

        groupNameEditText = v.findViewById(R.id.join_group_name_edittext);
        groupPasswordEditText = v.findViewById(R.id.join_group_password_edittext);

        db = DbHandler.getInstance();

        return v;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.join_group_button){
            String groupName = groupNameEditText.getText().toString().trim();
            String groupPassword = groupPasswordEditText.getText().toString();

            // Check if a field is empty and show warning if there is
            if(groupName.isEmpty() || groupPassword.isEmpty()){
                Toast.makeText(getContext(), "All fields must be filled in!", Toast.LENGTH_SHORT).show();
            }else{
                // Join group using DbHandler
                db.joinGroup(groupName, groupPassword);
                // Navigate to ProductsListFragment
                db.setUserState(User.UserState.PRODUCTS_LIST);
            }

        }
    }
}
