package svenliekens.drinksapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.fragments.dialogs.AmountDialog;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.handlers.ProductsAdapter;
import svenliekens.drinksapp.listeners.GroupDataChangedListener;
import svenliekens.drinksapp.listeners.GroupProductsChangedListener;
import svenliekens.drinksapp.listeners.ProductsListChangedListener;
import svenliekens.drinksapp.models.Product;
import svenliekens.drinksapp.models.Group;

/**
 * This is a fragment for a registered user who is part of a group.
 * This fragment is used to show the products with their amount and their price.
 * It also shows the total amount to pay and the date that this amount should be paid by.
 */
public class ProductsListFragment extends Fragment implements ProductsListChangedListener, GroupProductsChangedListener, GroupDataChangedListener{
    private DbHandler db;

    private ArrayList<Product> products;

    private TextView amountDueTextView;

    private ListView productsListView;
    private ProductsAdapter adapter;

    private String payDate = "00/0/0000";
    private double totalAmount = 0.0;

    public ProductsListFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_products_list, container, false);

        amountDueTextView = v.findViewById(R.id.amount_due_textview);
        productsListView = v.findViewById(R.id.products_listview);
        products = new ArrayList<>();

        db = DbHandler.getInstance();
        // Link listener to detect changes in user products
        db.linkUserProductsListener(this);
        // Link listener to detect changes in group pay date
        db.linkGroupPaydateChangedListener(this);
        // Link listener to detect changes in group products (price changes, deleted product, added product)
        db.syncGroupProductsWithUser(this);

        // Use custom ArrayAdapter to show products in ListView
        adapter = new ProductsAdapter(v.getContext(), R.layout.products_list_item, products);
        productsListView.setAdapter(adapter);
        // Open dialog to edit product amount when clicking a product in the ListView
        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product p = adapter.getItem(i);
                // popup to change product amount
                AmountDialog amountDialog = new AmountDialog(getActivity(), p);
                amountDialog.show();
            }
        });

        return v;
    }

    /**
     * This event is triggered when the user's products list has changed (example: product amount change)
     * @param productList ArrayList with updated productList
     */
    @Override
    public void productsListChanged(ArrayList<Product> productList) {
        if(productList!= null){
            // Update view with new data
            products = productList;
            adapter.clear();
            adapter.addAll(productList);

            // Recalculate total amount to pay
            for(Product p : productList){
                if(p.getAmount() != 0){
                    totalAmount += (((double)p.getAmount())* p.getPrice());
                }
            }
        }
    }

    /**
     * This event is triggered when the group's products have changed (examples: product added, product deleted, price updated)
     * @param groupProducts ArrayList with updated groupProducts
     */
    @Override
    public void groupProductsChanged(ArrayList<Product> groupProducts) {
        if(groupProducts != null && products != null) {
            // Check for price changes or new products
            for(Product groupProduct: groupProducts){
                boolean foundProduct = false;
                for(Product product: products){
                    // Check for price change on existing products
                    if(groupProduct.getName().equals(product.getName())){
                        if(product.getPrice() != groupProduct.getPrice()){
                            product.setPrice(groupProduct.getPrice());
                        }
                        foundProduct = true;
                    }
                }
                // If product wasn't found in old list, add it
                if(!foundProduct){
                    products.add(groupProduct);
                }
            }

            ArrayList<Product> toRemove = new ArrayList<>();
            // Find and remove deleted products
            for(Product product: products){
                boolean productNotFoundInGroupProducts = true;
                for(Product groupProduct: groupProducts){
                    if(product.getName().equals(groupProduct.getName())){
                        productNotFoundInGroupProducts = false;
                    }
                }
                if(productNotFoundInGroupProducts){
                    toRemove.add(product);
                }
            }
            products.removeAll(toRemove);

            // Update user products with updated products list
            db.updateUserProducts(products);
        }
    }

    /**
     * This event is triggerd when the group's payData has changed
     * @param group Group that contains all info
     */
    @Override
    public void groupDataChanged(Group group) {
        if(group != null){
            // Get new paydate from group
            payDate = group.getPayDate();
            // Update view
            amountDueTextView.setText("You have to pay €" + String.valueOf(totalAmount) + " before " + payDate);
        }
    }
}
