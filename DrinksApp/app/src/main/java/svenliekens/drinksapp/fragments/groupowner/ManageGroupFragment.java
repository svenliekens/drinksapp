package svenliekens.drinksapp.fragments.groupowner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.listeners.GroupDataChangedListener;
import svenliekens.drinksapp.models.Group;
import svenliekens.drinksapp.models.User;

/**
 * Fragment for group owner. This fragment contains a page with all the settings for a group
 */
public class ManageGroupFragment extends Fragment implements View.OnClickListener, GroupDataChangedListener{

    private EditText groupNameEditText;
    private EditText payDateEditText;
    private DbHandler db;

    public ManageGroupFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manage_group, container, false);

        payDateEditText = v.findViewById(R.id.manage_group_paydate_edittext);

        v.findViewById(R.id.manage_group_edit_products_button).setOnClickListener(this);
        v.findViewById(R.id.manage_group_save_button).setOnClickListener(this);
        v.findViewById(R.id.manage_group_all_payments_received_button).setOnClickListener(this);

        db = DbHandler.getInstance();
        // Get group data once and make this class listener for the event that goes with this method
        db.getGroupDataOnce(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.manage_group_edit_products_button:
                // Navigate to EditGroupProductsFragment when edit group products button is pressed
                db.setUserState(User.UserState.EDIT_GROUP_PRODUCTS);
                break;

            case R.id.manage_group_save_button:
                // Update the group data using the DbHandler when the save button is pressed
                db.updateGroupData(payDateEditText.getText().toString().trim());
                break;

            case R.id.manage_group_all_payments_received_button:
                // Reset all group members when all payments received button is pressed
                db.resetPaymentsInGroup();
                // Reset group user's products too
                db.resetUserProductsToGroupProducts();
                Toast.makeText(getContext(), "All payments reset.", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void groupDataChanged(Group group) {
        // Update view after getting group data (result of getGroupDataOnce in onCreateView)
        payDateEditText.setText(group.getPayDate());
    }
}
