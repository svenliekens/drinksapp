package svenliekens.drinksapp.fragments.groupowner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import svenliekens.drinksapp.EditProductActivity;
import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.handlers.ProductsAdapter;
import svenliekens.drinksapp.listeners.ProductsListChangedListener;
import svenliekens.drinksapp.models.Product;
import svenliekens.drinksapp.models.User;

/**
 * Fragment for the group manager that shows an overview of the group's products so they can be managed
 */
public class EditGroupProductsFragment extends Fragment implements View.OnClickListener, ProductsListChangedListener {

    private DbHandler db;

    private ListView productsListview;
    private ArrayList<Product> products;
    // Use custom adapter to show products in ListView
    private ProductsAdapter adapter;

    public EditGroupProductsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_group_products, container, false);

        v.findViewById(R.id.edit_group_products_add_product_button).setOnClickListener(this);

        productsListview = v.findViewById(R.id.group_products_listview);

        db = DbHandler.getInstance();
        // Make this class listener for the group's products list
        db.linkGroupProductsListener(this);

        products = new ArrayList<>();

        adapter = new ProductsAdapter(v.getContext(), R.layout.group_products_list_item, products);
        productsListview.setAdapter(adapter);
        // Start EditProductActivity when a product in the ListView is clicked
        productsListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product p = adapter.getItem(i);
                Intent editProductIntent = new Intent(getActivity(), EditProductActivity.class);
                editProductIntent.putExtra("product", (Serializable)p);
                startActivity(editProductIntent);
            }
        });

        return v ;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.edit_group_products_add_product_button){
            // Navigate to AddGroupProductFragment when the add product button is pressed
            db.setUserState(User.UserState.ADD_GROUP_PRODUCT);
        }
    }

    @Override
    public void productsListChanged(ArrayList<Product> productList) {
        // Update view when group products have changed
        products = productList;
        adapter.clear();
        adapter.addAll(productList);
    }
}
