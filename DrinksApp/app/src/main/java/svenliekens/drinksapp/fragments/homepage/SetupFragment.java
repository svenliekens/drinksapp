package svenliekens.drinksapp.fragments.homepage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.User;

/**
 * This is a fragment for a newly registered user.
 * This fragment is used to let the user choose between joining an existing group or creating a new group.
 */
public class SetupFragment extends Fragment implements View.OnClickListener{

    private TextView textView;

    private DbHandler db;

    public SetupFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setup, container, false);

        textView = v.findViewById(R.id.join_create_group_textview);

        v.findViewById(R.id.join_group_button).setOnClickListener(this);
        v.findViewById(R.id.create_group_button).setOnClickListener(this);

        db = DbHandler.getInstance();

        return v;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.join_group_button:
                // Navigate to JoinGroupFragment when join group button is pressed
                db.setUserState(User.UserState.JOIN_GROUP);
                break;

            case R.id.create_group_button:
                // Navigate to CreateGroupFragment when create group button is pressed
                db.setUserState(User.UserState.CREATE_GROUP);
                break;
        }
    }
}
