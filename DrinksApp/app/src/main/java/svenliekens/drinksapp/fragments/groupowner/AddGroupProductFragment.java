package svenliekens.drinksapp.fragments.groupowner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.Product;
import svenliekens.drinksapp.models.User;

/**
 * Fragment for group manager to add a new product to the group
 */
public class AddGroupProductFragment extends Fragment implements View.OnClickListener{

    private DbHandler db;

    private TextView productNameTextView;
    private TextView productPriceTextView;

    public AddGroupProductFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_group_product, container, false);

        db = DbHandler.getInstance();

        productNameTextView = v.findViewById(R.id.add_group_product_name_edittext);
        productPriceTextView = v.findViewById(R.id.add_group_product_price_edittext);

        v.findViewById(R.id.add_group_product_add_button).setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.add_group_product_add_button){
            String productName = productNameTextView.getText().toString().trim();
            String productPrice = productPriceTextView.getText().toString().trim();

            // Check if one of the fields is empty, show toast if there is
            if(productPrice.isEmpty() || productName.isEmpty()){
                Toast.makeText(view.getContext(), "All fields must be filled in!", Toast.LENGTH_SHORT).show();
            }else{
                // Create new product from input fields
                Product productToAdd = new Product(productName, Double.parseDouble(productPrice));
                // Put new product in db using DbHandler
                db.addGroupProduct(productToAdd);
                // Change the UserState so the user goes back to the EditGroupProductsFragment
                db.setUserState(User.UserState.EDIT_GROUP_PRODUCTS);
            }
        }
    }
}
