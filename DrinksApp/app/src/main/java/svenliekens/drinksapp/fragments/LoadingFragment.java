package svenliekens.drinksapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import svenliekens.drinksapp.R;

/**
 * This fragment is used to show a loading screen
 */
public class LoadingFragment extends Fragment {

    public LoadingFragment(){ }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loading, container, false);
    }
}
