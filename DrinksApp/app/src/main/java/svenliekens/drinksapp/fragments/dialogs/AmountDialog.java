package svenliekens.drinksapp.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import svenliekens.drinksapp.R;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.Product;

/**
 * Created by Sven on 21/01/2018.
 */

/**
 * Custom Dialog to adjust amount of products the user has bought/consumed
 */
public class AmountDialog extends Dialog implements android.view.View.OnClickListener {

    private TextView amountTextView;
    private TextView productNameTextView;
    private DbHandler db;

    private Product p;
    private int amount = 0;

    public AmountDialog(Activity a, Product p) {
        super(a);
        this.p = p;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.amount_dialog);

        findViewById(R.id.amount_save_button).setOnClickListener(this);
        findViewById(R.id.minus_button).setOnClickListener(this);
        findViewById(R.id.plus_button).setOnClickListener(this);

        productNameTextView = findViewById(R.id.product_name_label);
        productNameTextView.setText(p.getName());

        amountTextView = findViewById(R.id.amount_textview);
        amount = p.getAmount();
        amountTextView.setText(String.valueOf(amount));

        db = DbHandler.getInstance();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.minus_button:
                // Lower product amount when minus-button is pressed
                if(amount > 0){
                    amount--;
                    p.setAmount(amount);
                    amountTextView.setText(String.valueOf(amount));
                }
                break;

            case R.id.plus_button:
                // Raise product amount when plus-button is pressed
                amount++;
                p.setAmount(amount);
                amountTextView.setText(String.valueOf(amount));
                break;

            case R.id.amount_save_button:
                // Update the product in the database using the DbHandler
                db.updateUserProduct(p);
                dismiss();
                break;
        }
    }


}
