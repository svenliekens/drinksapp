package svenliekens.drinksapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * This activity is used to log the user in using Firebase Auth
 */
public class LogInActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseDatabase db;
    private DatabaseReference usersRef;
    private FirebaseAuth auth;

    private EditText emailEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        db = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();

        (findViewById(R.id.register_button)).setOnClickListener(this);
        (findViewById(R.id.login_button)).setOnClickListener(this);

        emailEditText = findViewById(R.id.login_email_edittext);
        passwordEditText = findViewById(R.id.login_password_edittext);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.register_button:
                navToRegisterPage();
                break;

            case R.id.login_button:
                logIn();
                break;
        }
    }

    /**
     * Method to navigate to register page
     */
    private void navToRegisterPage() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    /**
     * Method to navigate to MainActivity
     */
    private void navToMainPage() {
        Intent mainPageIntent = new Intent(this, MainActivity.class);
        startActivity(mainPageIntent);
    }

    /**
     * Method to authenticate with Firebase Authentication
     */
    private void logIn() {
        auth.signInWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString())
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("LOG IN", "signInWithEmail:success");

                    FirebaseUser user = auth.getCurrentUser();

                    if(user != null){
                        Toast.makeText(LogInActivity.this, "Authenticated: " + user.getEmail(), Toast.LENGTH_SHORT).show();
                        // navigate
                        navToMainPage();
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("LOG IN", "signInWithEmail:failure", task.getException());
                    Toast.makeText(LogInActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
