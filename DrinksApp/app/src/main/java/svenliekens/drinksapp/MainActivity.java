package svenliekens.drinksapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;

import svenliekens.drinksapp.fragments.ProductsListFragment;
import svenliekens.drinksapp.fragments.groupowner.AddGroupProductFragment;
import svenliekens.drinksapp.fragments.groupowner.EditGroupProductsFragment;
import svenliekens.drinksapp.fragments.groupowner.GroupOwnerHomePageFragment;
import svenliekens.drinksapp.fragments.groupowner.ManageGroupFragment;
import svenliekens.drinksapp.fragments.homepage.CreateGroupFragment;
import svenliekens.drinksapp.fragments.LoadingFragment;
import svenliekens.drinksapp.fragments.homepage.PaymentReceivedFragment;
import svenliekens.drinksapp.fragments.homepage.SetupFragment;
import svenliekens.drinksapp.fragments.homepage.JoinGroupFragment;
import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.listeners.UserDataChangedListener;
import svenliekens.drinksapp.models.User;

/**
 * This activity is used to manage all fragments depending on the userstate saved in the database
 */
public class MainActivity extends AppCompatActivity implements UserDataChangedListener {
    private FirebaseAuth auth;

    private User currentUser;
    private DbHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoadingFragment()).commit();

        auth = FirebaseAuth.getInstance();
        checkAuthState();

        db = DbHandler.getInstance();
        db.linkUserDataListener(this);
    }

    /**
     * This method is used to take the user to the login screen if he's not logged in
     */
    public void checkAuthState() {
        if(auth.getCurrentUser() == null){
            Intent signInIntent = new Intent(this, LogInActivity.class);
            startActivity(signInIntent);
        }
    }

    /**
     * This event is fired when the user data has changed.
     * It creates a different fragment based on the users's state and displays this fragment
     * @param u
     */
    @Override
    public void userDataChanged(User u) {
        currentUser = u;

        if (findViewById(R.id.fragment_container) != null) {
            Fragment fragment = null;
            switch(u.getState()){
                case PAYMENT_RECEIVED:
                    fragment = new PaymentReceivedFragment();
                    break;

                case ADD_GROUP_PRODUCT:
                    fragment = new AddGroupProductFragment();
                    break;

                case EDIT_GROUP_PRODUCTS:
                    fragment = new EditGroupProductsFragment();
                    break;

                case JOIN_GROUP:
                    fragment = new JoinGroupFragment();
                    break;

                case CREATE_GROUP:
                    fragment = new CreateGroupFragment();
                    break;

                case NOT_ASSIGNED:
                    fragment = new SetupFragment();
                    break;

                case GROUP_OWNER_HOMEPAGE:
                    fragment = new GroupOwnerHomePageFragment();
                    break;

                case MANAGE_GROUP:
                    fragment = new ManageGroupFragment();
                    break;

                case GROUP_OWNER_PRODUCTS_LIST:
                case PRODUCTS_LIST:
                    fragment = new ProductsListFragment();
                    break;
            }
            if(fragment != null){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commitAllowingStateLoss();
            }
        }
    }

    /**
     * Used to create options menu with logout button based on layout
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    /**
     * Log out the user when the logout button in the options menu is pressed
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_logout_button:
                auth.signOut();
                checkAuthState();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Navigate to previous logical fragment when user presses back button.
     * This is based on the current user state. Going back means navigating to another fragment.
     */
    @Override
    public void onBackPressed() {
        switch(currentUser.getState()){
            case JOIN_GROUP:
            case CREATE_GROUP:
                db.setUserState(User.UserState.NOT_ASSIGNED);
                break;

            case ADD_GROUP_PRODUCT:
                db.setUserState(User.UserState.EDIT_GROUP_PRODUCTS);
                break;

            case EDIT_GROUP_PRODUCTS:
                db.setUserState(User.UserState.MANAGE_GROUP);
                break;

            case MANAGE_GROUP:
            case GROUP_OWNER_PRODUCTS_LIST:
                db.setUserState(User.UserState.GROUP_OWNER_HOMEPAGE);
                break;

            case PAYMENT_RECEIVED:
            case PRODUCTS_LIST:
            case GROUP_OWNER_HOMEPAGE:
            case NOT_ASSIGNED:
                finishAffinity();
                break;
        }
    }
}