package svenliekens.drinksapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import svenliekens.drinksapp.handlers.DbHandler;
import svenliekens.drinksapp.models.User;

/**
 * This activity is used to register a new user using Firebase Authentication
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private FirebaseAuth auth;
    private FirebaseDatabase db;

    private EditText usernameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();

        usernameEditText = findViewById(R.id.register_username_edittext);
        emailEditText = findViewById(R.id.register_email_edittext);
        passwordEditText = findViewById(R.id.register_password_edittext);

        (findViewById(R.id.reg_button)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.reg_button){
            registerUser();
        }
    }

    /**
     * Method to register a new user using Firebase Authentication
     */
    private void registerUser() {
        auth.createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString())
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("REGISTER", "createUserWithEmail:success");

                    FirebaseUser user = auth.getCurrentUser();

                    if(user != null){
                        DatabaseReference userReference = db.getReference("users").child(user.getUid());
                        User newUser = new User(user.getUid(), usernameEditText.getText().toString().trim());
                        userReference.setValue(newUser);

                        Toast.makeText(RegisterActivity.this, "Registered: "+ user.getEmail(), Toast.LENGTH_SHORT).show();
                        // navigate
                        navToMainPage();
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("REGISTER", "createUserWithEmail:failure", task.getException());
                    Toast.makeText(RegisterActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();

                    // don't navigate
                }
            }
        });
    }

    /**
     * Method to navigate to MainActivity after registering
     */
    private void navToMainPage() {
        Intent mainPageIntent = new Intent(this, MainActivity.class);
        startActivity(mainPageIntent);
    }
}
